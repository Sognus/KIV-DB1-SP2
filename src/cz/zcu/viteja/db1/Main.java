package cz.zcu.viteja.db1;

import java.awt.GraphicsEnvironment;
import java.io.*;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	private Scanner scan;
	private MySQL mysql;

	public static void main(String[] args) throws IOException {

		Console console = System.console();
		if(console == null && !GraphicsEnvironment.isHeadless()){
			String filename = Main.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(6);
			Process p1 = Runtime.getRuntime().exec("cmd /c start start.bat");

		}else{
			new Main();
		}

	}

	private Main() throws UnsupportedEncodingException {
		this.mysql = null;
		this.scan = new Scanner(System.in);

		this.mainLoop();
	}

	private void mainLoop() {

		// Pokusit se připojit z dat ze souboru
		try {
			File conf = new File("connection.txt");

			if (conf.exists()) {
				String hostname;
				String port;
				String database;
				String user;
				String password = "";

				String[] data = new String[5];
				int pointer = 0;

				FileReader fr = new FileReader(conf);
				BufferedReader br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					data[pointer] = line;
					pointer++;

					if (pointer >= 4) {
						break;
					}
				}

				hostname = data[0];
				port = data[1];
				database = data[2];
				user = data[3];
				password = data[4];

				this.mysql = new MySQL(hostname, port, database, user, password);
				this.mysql.openConnection();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		while (true) {

			this.printMenu();

			String s = this.scan.nextLine();

			try {
				switch (s) {
				case "1":
					this.checkConnection();
					break;
				case "2":
					this.createConnection();
					break;
				case "3":
					this.refreshConnection();
					break;
				case "4":
					this.disconnect();
					break;
				case "5":
					this.insertCity();
					break;
				case "6":
					this.deleteCity();
					break;
				case "7":
					this.printCities();
					break;
				case "8":
					this.printCitiesConditional();
					break;
				default:
					System.out.println();
					System.out.println("Neznámá akce!");

				}
			} catch (Exception e) {
				System.out.println("Při zpracování požadavku vznikla chyba: " + e.getMessage());
			}

			System.out.println("-----");
			System.out.println();

		}

	}

	private void printCitiesConditional() throws SQLException {
		if (this.mysql == null || !this.mysql.checkConnection()) {
			System.out.println("Aplikace není připojena k databázi!");
			return;
		}

		if (!this.createStructure()) {
			System.out.println("Databázová struktura neexistuje a nemohla být vytvořena!");
			return;
		}

		System.out.println("[Filtrovat dle počtu obyvatel]");

		System.out.print("Podmínka (>, <, =): ");
		String podminka = scan.nextLine();

		String sql = "";

		switch (podminka) {
		case ">":
			sql = "SELECT * FROM viteja_zk_mesto WHERE obyvatel > ?";
			break;
		case "<":
			sql = "SELECT * FROM viteja_zk_mesto WHERE obyvatel < ?";
			break;
		case "=":
			sql = "SELECT * FROM viteja_zk_mesto WHERE obyvatel = ?";
			break;
		default:
			System.out.println("Neznámá podmínka!");
			return;
		}

		System.out.print("číslo: ");
		Integer pocet = scan.nextInt();
		scan.nextLine();

		System.out.println("-");

		java.sql.PreparedStatement pstat = this.mysql.getConnection().prepareStatement(sql);
		pstat.setInt(1, pocet);

		ResultSet rset = pstat.executeQuery();

		while (rset.next()) {
			System.out.println(String.format("Číslo: %d", rset.getInt("c_mesta")));
			System.out.println(String.format("Název: %s", rset.getString("nazev")));
			System.out.println(String.format("Obyvatel: %d", rset.getInt("obyvatel")));
			System.out.println("-");
		}

	}

	private void deleteCity() throws SQLException {
		if (this.mysql == null || !this.mysql.checkConnection()) {
			System.out.println("Aplikace není připojena k databázi!");
			return;
		}

		if (!this.createStructure()) {
			System.out.println("Databázová struktura neexistuje a nemohla být vytvořena!");
			return;
		}

		System.out.println("[Smazat město]");

		System.out.print("Jméno: ");
		String nazev = scan.nextLine().toLowerCase().trim();

		java.sql.PreparedStatement pstat = this.mysql.getConnection()
				.prepareStatement("DELETE FROM `viteja_zk_mesto` WHERE `viteja_zk_mesto`.`nazev` = ?");
		pstat.setString(1, nazev);
		pstat.execute();

	}

	private void printCities() throws SQLException {
		if (this.mysql == null || !this.mysql.checkConnection()) {
			System.out.println("Aplikace není připojena do databáze!");
			return;
		}

		if (!this.createStructure()) {
			System.out.println("Databázová struktura neexistuje a nemohla být vytvořena!");
			return;
		}

		System.out.println("[Města]");

		java.sql.PreparedStatement pstat = this.mysql.getConnection()
				.prepareStatement("SELECT * FROM `viteja_zk_mesto`");
		ResultSet rset = pstat.executeQuery();

		System.out.println("-");

		while (rset.next()) {
			System.out.println(String.format("Číslo: %d", rset.getInt("c_mesta")));
			System.out.println(String.format("Název: %s", rset.getString("nazev")));
			System.out.println(String.format("Obyvatel: %d", rset.getInt("obyvatel")));
			System.out.println("-");
		}

	}

	private void insertCity() throws SQLException, UnsupportedEncodingException {
		if (this.mysql == null || !this.mysql.checkConnection()) {
			System.out.println("Aplikace není připojena k databázi!");
			return;
		}

		if (!this.createStructure()) {
			System.out.println("Databázová struktura neexistuje a nemohla být vytvořena!");
			return;
		}

		System.out.println("[Město]");

		System.out.print("Číslo: ");
		Integer cislo = scan.nextInt();
		scan.nextLine();

		System.out.print("Název: ");
		String nazev = scan.nextLine();

		System.out.print("Obyvatel: ");
		Integer obyvatel = scan.nextInt();
		scan.nextLine();

		java.sql.PreparedStatement pstat = this.mysql.getConnection()
				.prepareStatement("INSERT INTO `viteja_zk_mesto` (`c_mesta`, `nazev`, `obyvatel`) VALUES (? , ?, ?)");

		pstat.setInt(1, cislo);
		pstat.setString(2, nazev);
		pstat.setInt(3, obyvatel);

		pstat.execute();

	}

	private void disconnect() throws SQLException {
		if (this.mysql != null) {
			this.mysql.closeConnection();
			System.out.println("Spojení s databází bylo ukončeno!");
		}

	}

	private void printMenu() {

		ArrayList<String> menuItems = new ArrayList<String>();

		// Definice položek menu
		menuItems.add("[MySQL]");
		menuItems.add("Zkontrolovat připojení k MySQL databázi");
		menuItems.add("Vytvořit spojení s MySQL databází");
		menuItems.add("Obnovit spojení s MySQL databází");
		menuItems.add("Ukončit spojení s MySQL databází");
		menuItems.add("---");
		menuItems.add("Vložit nové město");
		menuItems.add("Smazat město dle jména");
		menuItems.add("Výpis měst");
		menuItems.add("Filtrace měst dle počtu obyvatel");

		int a = 1;

		for (int i = 0; i < menuItems.size(); i++) {
			String s = menuItems.get(i);

			if (!s.startsWith("-") && !s.startsWith("[")) {
				System.out.println(String.format(" %d - %s", a, s));
				a++;
			} else {
				System.out.println(s);
			}

		}

		System.out.println();
		System.out.print("Zvolte akci: ");

	}

	private void checkConnection() throws SQLException {
		if (this.mysql == null || !this.mysql.checkConnection()) {
			System.out.println("Aplikace NENÍ připojena k databázi!");
			return;
		}

		System.out.println("Aplikace JE připojena k databázi!");

	}

	private void createConnection() throws SQLException, ClassNotFoundException {

		boolean createNew = false;

		if (this.mysql == null) {
			createNew = true;
		} else {

			System.out
					.println("Připojení k databázi již bylo provedeno, chcete se pokusit jej obnovit? (default: ano)");
			System.out.print("volba: ");

			String volba = scan.nextLine().toLowerCase();

			switch (volba) {
			case "a":
			case "ano":
				createNew = false;
				break;
			case "ne":
			case "n":
				createNew = true;
				break;
			default:
				createNew = false;
			}

		}

		if (createNew) {
			System.out.println();
			System.out.println("[MySQL]");

			System.out.print("Adresa serveru: ");
			String host = scan.nextLine();

			System.out.print("Port serveru: ");
			String port = String.valueOf(scan.nextInt());
			scan.nextLine();

			System.out.print("Jméno databáze: ");
			String database = scan.nextLine();

			System.out.print("Uživatel: ");
			String user = scan.nextLine();

			System.out.print("Heslo: ");
			String password = scan.nextLine();

			this.mysql = new MySQL(host, port, database, user, password);
			this.mysql.openConnection();

			System.out.println();

			if (this.mysql.checkConnection()) {
				System.out.println("Spojení s databází úspěšně navázáno!");
				return;
			}

			System.out.println("Nepodařilo se vytvořit spojení s databází!");
		} else {
			this.mysql.openConnection();

			if (!this.mysql.checkConnection()) {
				System.out.println("Nelze obnovit spojení s databází! Je nutné se připojit znovu!");
			} else {
				System.out.println("Spojení s databází bylo úspěšně obnoveno!");
			}
		}

	}

	private void refreshConnection() throws ClassNotFoundException, SQLException {
		if (this.mysql == null) {
			this.createConnection();
		} else {

			if (!this.mysql.checkConnection()) {
				this.mysql.openConnection();
			}

			if (this.mysql.checkConnection()) {
				System.out.println("Spojení s databází úspěšně navázáno!");
				return;
			}

			this.createConnection();
		}
	}

	private boolean createStructure() throws SQLException {
		if (this.mysql == null || !this.mysql.checkConnection()) {
			return false;
		}

		// Zjištění zda tabulka existuje
		DatabaseMetaData dbm = this.mysql.getConnection().getMetaData();
		ResultSet tables = dbm.getTables(null, null, "viteja_zk_mesto", null);

		// Tabulka existuje
		if (tables.next()) {
			return true;
		} else {
			// Tabulka neexistuje, pokus o vytvoření
			String sql = "CREATE TABLE IF NOT EXISTS `viteja_zk_mesto` (\r\n" + "  `c_mesta` bigint(20) NOT NULL,\r\n"
					+ "  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL,\r\n" + "  `obyvatel` int(11) NOT NULL,\r\n"
					+ "  PRIMARY KEY (`c_mesta`),\r\n" + "  UNIQUE KEY `nazev` (`nazev`)\r\n"
					+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;";

			java.sql.PreparedStatement pstat = this.mysql.getConnection().prepareStatement(sql);
			pstat.execute();
		}

		// Ověření vytvoření tabulky
		DatabaseMetaData dbm2 = this.mysql.getConnection().getMetaData();
		ResultSet tables2 = dbm2.getTables(null, null, "viteja_zk_mesto", null);

		// Tabulka vytvořena
		if (tables2.next()) {
			return true;
		}

		// Tabulka nebyla vytvořena
		return false;

	}

}
