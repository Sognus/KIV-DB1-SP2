package cz.zcu.viteja.db1;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 
 * Abstraktní třída pro připojení do databáze. Slouží jako základ pro jakékoliv
 * typy databází (MySQL, SQLite, ...)
 * 
 * 
 */
public abstract class Database {

	/** Připojení do databáze */
	protected Connection connection;

	/**
	 * Konstruktor databázového objektu
	 *
	 */
	protected Database() {
		this.connection = null;
	}

	/**
	 * 
	 * 
	 * Vytvoří nové spojení s vyžadovanou databází
	 * 
	 * @return vytvořené spojení
	 * @throws SQLException
	 *             V případě, že spojení nemmůže být navázáno
	 * @throws ClassNotFoundException
	 *             v případě, že ovladač pro připojení k databázi není nalezen
	 */
	public abstract Connection openConnection() throws SQLException, ClassNotFoundException;

	/**
	 * 
	 * Zkontroluje, zda je aplikace připojena k databázi
	 * 
	 * @return true pokud je spojení s databází navázáno, jinak false
	 * @throws SQLException
	 *             pokud připojení nemůže být ověřeno
	 */
	public boolean checkConnection() throws SQLException {
		if (connection == null || connection.isClosed()) {
			return false;
		}

		try {
			java.sql.PreparedStatement pstat = connection.prepareStatement("SELECT 1");
			pstat.executeQuery();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * Získání aktuálního spojení s databází
	 * 
	 * @return Aktuální spojení s databází, null pokud aplikace není připojena
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * Ukončí spojení s databází
	 * 
	 * @return true, pokud bylo ukončení spojení úspěné
	 * @throws SQLException
	 *             v případě, že spojení nelze ukončit
	 */
	public boolean closeConnection() throws SQLException {
		if (connection == null) {
			return false;
		}
		connection.close();
		return true;
	}

}