package cz.zcu.viteja.db1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL extends Database {

	private final String user;
	private final String database;
	private final String password;
	private final String port;
	private final String hostname;

	/**
	 * Vytvoí novou instanci pro p�ipojen� do MySQL datab�ze
	 *
	 * @param hostname
	 *            IP/doména, na která beží server MySQL
	 * @param port
	 *            Port, na kterém běží MySQL server
	 * @param database
	 *            Jméno databáze
	 * @param username
	 *            Použité uživatelské jméno
	 * @param password
	 *            Použité heslo
	 */
	public MySQL(String hostname, String port, String database, String username, String password) {
		this.hostname = hostname;
		this.port = port;
		this.database = database;
		this.user = username;
		this.password = password;
	}

	@Override
	public Connection openConnection() throws SQLException, ClassNotFoundException {
		if (checkConnection()) {
			return connection;
		}

		String connectionURL = "jdbc:mysql://" + this.hostname + ":" + this.port;
		if (database != null) {
			connectionURL = connectionURL + "/" + this.database
					+ "?autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=utf-8";
		}

		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection(connectionURL, this.user, this.password);
		return connection;
	}

}
